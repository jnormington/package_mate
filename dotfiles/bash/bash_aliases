# Shortcuts
alias hm="cd ~"
alias cdr="cd ~/Repository"
alias ..="cd .."
alias ...="cd ../.."
alias -- -="cd -"

alias hist="history"
alias lsd="ls -ltGF"
alias fdr="open ./"
alias be="bundle exec"

alias g="git"
alias gs="git status"
alias gb="git branch"
alias gp="git pull"
alias gd="git diff"
alias gc="git checkout"
alias gcb="git checkout -b"

alias v="sublime ./"
alias vv="sublime"
alias findit="show && open ./"

alias traceit="sudo dtrace -p"
alias less="less -X" # Don't clear the screen init

# Get date specifics
alias year='date +%G'
alias week='date +%V'
alias day='date +%A'
alias month='date +%B'
alias current='date +%X'

# Below mostly from https://github.com/mathiasbynens

# Get OS X Software Updates, as well as brew
alias update_all='sudo softwareupdate -i -a; brew update; brew upgrade --all'

# IP addresses
alias eip="dig +short myip.opendns.com @resolver1.opendns.com"
alias iip="ipconfig getifaddr en0"

# Clean up LaunchServices to remove duplicates in the "Open with" context menu
alias lscleanup="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user && killall Finder"

# Empty the Trash on all mounted volumes and the main HDD.
# Also, clear Apple’s System Logs to improve shell startup speed.
# Finally, clear download history from quarantine. https://mths.be/bum
alias emptytrash="sudo rm -rfv /Volumes/*/.Trashes; sudo rm -rfv ~/.Trash; sudo rm -rfv /private/var/log/asl/*.asl; sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'delete from LSQuarantineEvent'"

# Show/hide hidden files in Finder
alias show="defaults write com.apple.finder AppleShowAllFiles -bool true && killall Finder"
alias hide="defaults write com.apple.finder AppleShowAllFiles -bool false && killall Finder"

# Hide/show desktop icons
alias hidedesktop="defaults write com.apple.finder CreateDesktop -bool false && killall Finder"
alias showdesktop="defaults write com.apple.finder CreateDesktop -bool true && killall Finder"

# Spotlight
alias spotoff="sudo mdutil -a -i off"
alias spoton="sudo mdutil -a -i on"

# Volume
alias mutet="osascript -e 'set volume output muted true'"
alias mutef="osascript -e 'set volume output muted false'"

# Lock the screen (when going AFK)
alias afk="/System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend"

# Reload the shell (i.e. invoke as a login shell)
alias reload="exec $SHELL -l"
